import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import  *  as  data  from  '../../assets/files/data.json';

export interface RecordEmployee{
  email: string;
  first_name: string;
  last_name: string;
  id: number;
  n_document: number;
  phone_number: number;
}

const ELEMENT_DATA: RecordEmployee[] =  (data as any).default;

@Component({
  selector: 'app-root',
  templateUrl: './prueba.component.html',
  styleUrls: ['./prueba.component.sass']
})

export class PruebaComponent implements OnInit {

	displayedColumns: string[] = ['id', 'n_document', 'first_name', 'last_name', 'email', 'phone_number'];
	dataSource = ELEMENT_DATA;
	dataSourceOriginal = ELEMENT_DATA;
	
  
  constructor() { }

  ngOnInit(): void {
  	//console.log(this.dataSource);
  	this.dataSource.map((record) => console.log(record.email));
  }

  public applyFilter(value:string){
  	if(value != ''){
  		this.dataSource = this.dataSourceOriginal.filter(record =>  String(record.id).indexOf(value) != -1 
  			|| String(record.n_document).indexOf(value) != -1 || record.first_name.indexOf(value) != -1
  			|| record.last_name.indexOf(value) != -1 || record.email.indexOf(value) != -1 
  			|| String(record.phone_number).indexOf(value) != -1);
  	}else{
  		this.dataSource = this.dataSourceOriginal;
  	}
  }

}
